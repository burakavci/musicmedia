module burak-avci/musicapp

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.16
)
