package Models

type User struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	FirstName string `json:"firstname" gorm:"column:firstname"`
	LastName  string `json:"lastname" gorm:"column:lastname"`
}

type UserAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthResult struct {
	Success  bool
	JWTToken string
}
