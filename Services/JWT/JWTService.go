package Services

import (
	Settings "burak-avci/musicapp/Services/Settings"
	"time"

	"github.com/golang-jwt/jwt"
)

type JWTEngine interface {
	GenerateToken(username string) (string, error)
	ValidateToken(token string) (bool, error)
}

type JWTClaim struct {
	Username string
	*jwt.StandardClaims
}

type JWTService struct{}

func NewJWTService() JWTEngine {
	return &JWTService{}
}

func (jwtService *JWTService) GenerateToken(username string) (string, error) {
	secret := Settings.GetSecretKey()

	claims := &JWTClaim{
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
		Username: username,
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = *claims

	return token.SignedString(secret)
}

func (jwtService *JWTService) ValidateToken(token string) (bool, error) {
	var claims JWTClaim
	parsedToken, err := jwt.ParseWithClaims(token, &claims, func(t *jwt.Token) (interface{}, error) { return Settings.GetSecretKey(), nil })
	if err != nil {
		return false, err
	}
	return parsedToken.Valid, nil
}
